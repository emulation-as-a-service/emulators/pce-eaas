FROM phusion/baseimage:0.9.22
CMD ["/sbin/my_init"]

LABEL "EAAS_EMULATOR_TYPE"="pce"
LABEL "EAAS_EMULATOR_VERSION"="bwfla-legacy"

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get upgrade -y --force-yes --option='Dpkg::Options::=--force-confnew'

## init bwfla repo
COPY files/bwfla.list /etc/apt/sources.list.d/bwfla.list
COPY files/pin-bwfla.pref /etc/apt/preferences.d/pin-bwfla.pref

run echo "deb [trusted=yes] http://winswitch.org/ xenial main" > /etc/apt/sources.list.d/winswitch.list
run apt-add-repository ppa:fengestad/stable

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes \
pce-dos \
pce-ibmpc \
pce-ibmpc-data \
pce-utils \
pce-atari-st \
pce-atari-st-data \
pce-macplus \
pce-macplus-data \
libx11-dev \
       libxext-dev \
       libxt-dev \
       libxv-dev \
       x11proto-core-dev \
       libaudiofile-dev \
       libpulse-dev \
       libgl1-mesa-dev \
       libasound2-dev \
       libcaca-dev \
       libglu1-mesa-dev \
       libxkbcommon-dev \
       xpra=2.3.4-r20525-1 \
       python-pip python-dev python-netifaces  python-avahi  pulseaudio socat

copy emucon-init /usr/bin/
run addgroup --gid 1000 bwfla
run useradd -ms /bin/bash --uid 1000 --gid bwfla bwfla
run adduser bwfla xpra
run sed -i '$s/$/ -nolisten local/' /etc/xpra/conf.d/55_server_x11.conf
run sed -i '$s/-auth *[^ ]*//' /etc/xpra/conf.d/55_server_x11.conf

run mkdir -p /run/user/1000/xpra
run chmod a=rwx /run/user/1000/xpra

run mkdir -p /home/bwfla
run chmod a=rwx /home/bwfla
